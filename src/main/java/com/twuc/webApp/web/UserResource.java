package com.twuc.webApp.web;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;

public class UserResource extends Resource<User> {
    public UserResource(User content, Link... links) {
        super(content, links);
    }
}
